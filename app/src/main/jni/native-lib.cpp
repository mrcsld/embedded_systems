#include <jni.h>
#include <string>
#include <stdio.h>
#include <android/log.h>
#include <stdlib.h>

#include "include/aubio/aubio.h"

#define  LOG_TAG    "GyroSniffer"
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)


extern "C"
{

    // Function to process each block
    void process_block (aubio_mfcc_t* mfcc, aubio_pvoc_t* pv, cvec_t* fftgrain, fvec_t *ibuf, fvec_t *obuf)
    {
      fvec_zeros(obuf);
      //compute mag spectrum
      aubio_pvoc_do(pv, ibuf, fftgrain);
      //compute mfccs
      aubio_mfcc_do(mfcc, fftgrain, obuf);
    }

    JNIEXPORT jobject

        JNICALL
        Java_it_unipd_dei_gyrosniffer_NativeMethods_getFeatures(
                JNIEnv *env,
                jobject,
                jstring path)  {

    uint_t samplerate = 0; // obtains sample rate of the file
    uint_t hop_size = 256;
    uint_t read = 0;

    uint_t win_s = 512; // fft size
    uint_t n_filters = 40; // number of filters
    uint_t n_coefs = 13; // number of coefficients
    cvec_t *in = new_cvec (win_s); // input buffer
    fvec_t *out = new_fvec (n_coefs); // output coefficients
    cvec_t *fftgrain = new_cvec (win_s); // filters used in the block processing
    // phase vocalizer, should improve the audio quality
    aubio_pvoc_t *pv = new_aubio_pvoc (win_s, hop_size); // phase vocalizer, should improve the audio quality

    // JNI calls
    jclass floatClass = env->FindClass("[F");
    jclass arrayClass = env->FindClass("java/util/ArrayList");
    jmethodID mid_init =  env->GetMethodID(arrayClass, "<init>", "()V");
    jobject objArr = env->NewObject(arrayClass, mid_init);
    jmethodID mid_add = env->GetMethodID(arrayClass, "add", "(Ljava/lang/Object;)Z");
    jfloatArray floatArray;
    // Opening the file
    const char_t *source_path = env->GetStringUTFChars(path, JNI_FALSE);
    aubio_source_t* s = new_aubio_source(source_path, samplerate, hop_size);

    if (!s) {return (jobject)source_path; }
    fvec_t *vec = new_fvec(hop_size);
    samplerate = aubio_source_get_samplerate(s);
    LOGD("SAMPLERATE: %d, SOURCE_PATH: %s", samplerate, source_path);
    aubio_mfcc_t *mfcc = new_aubio_mfcc (win_s, n_filters, n_coefs, samplerate);
    do {
        aubio_source_do(s, vec, &read);
        process_block(mfcc, pv, fftgrain, vec, out);

        //add to the output
        floatArray = env->NewFloatArray(n_coefs);
        env->SetFloatArrayRegion(floatArray, 0, n_coefs, (float*)out->data);
        env->CallBooleanMethod(objArr, mid_add, floatArray);
    } while ( read == hop_size );

    // calling the deconstructors
    aubio_source_close(s);
    del_fvec (vec);
    del_aubio_source (s);
    del_aubio_mfcc(mfcc);

    return objArr;
    }
}
