LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

#---- FOR X86 ----
#LOCAL_MODULE := libaubio
#LOCAL_SRC_FILES := ../jniLibs/x86/libaubio.a
#LOCAL_C_INCLUDES := ../jniLibs/x86/include

#---- FOR ARM ----
LOCAL_MODULE := libaubio
LOCAL_SRC_FILES := ../jniLibs/armeabi-v7a/libaubio.a
LOCAL_C_INCLUDES := ../jniLibs/armeabi-v7a/include

include $(PREBUILT_STATIC_LIBRARY)
include $(CLEAR_VARS)

LOCAL_LDLIBS := -llog
LOCAL_MODULE := native-lib
LOCAL_SRC_FILES := native-lib.cpp



LOCAL_STATIC_LIBRARIES := libaubio


include $(BUILD_SHARED_LIBRARY)