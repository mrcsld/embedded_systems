package it.unipd.dei.gyrosniffer;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;


import umich.cse.yctung.androidlibsvm.LibSVM;


public class MainActivity extends AppCompatActivity {
    private NativeMethods mNativeMethods = new NativeMethods();

    private final String TAG = "GyroSniffer";
    private SensorManager mSensorManager;
    private Sensor mGyroscope;
    private Button mRecordButton;
    private PrintWriter mPrintWriter;
    private TextView mTextView;
    private boolean recording = false;
    private final int GYRO_SAMPLE_RATE = 200;
    private final String GYRO_SAMPLES_DIR = Environment.getExternalStorageDirectory().getAbsolutePath();
    private final String GYRO_SAMPLES_FILE = Environment.getExternalStorageDirectory().getAbsolutePath() + "/gyro_samples.txt";
    private final String SVM_TRAIN_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + "/libsvm/train";
    private final String SVM_MALE_SAMPLES_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + "/libsvm/samples/male";
    private final String SVM_FEMALE_SAMPLES_DIR = Environment.getExternalStorageDirectory().getAbsolutePath() + "/libsvm/samples/female";
    private final String SVM_MALE_OUT_WAV = Environment.getExternalStorageDirectory().getAbsolutePath() + "/libsvm/samples/male_wav";
    private final String SVM_FEMALE_OUT_WAV = Environment.getExternalStorageDirectory().getAbsolutePath() + "/libsvm/samples/female_wav";
    private final int MY_PERMISSIONS_REQUEST_STORAGE = 1;

    private File file;

    LibSVM svm = new LibSVM();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Here, thisActivity is the current activity
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) || (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_STORAGE);
        }


        //convertAllToSVMFormat();
        //trainSVM();
        //getTrainAccuracy();
        //validateModel();

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        mTextView = (TextView) findViewById(R.id.result_text);

        mRecordButton = (Button)findViewById(R.id.record_button);
        mRecordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(recording){
                    mSensorManager.unregisterListener(onSensorChange);
                    mPrintWriter.close();
                    String ciao = predictClassOf();
                    mTextView.setText(ciao);
                    mRecordButton.setText(R.string.when_recording);

                } else {
                    try{
                        file = new File(GYRO_SAMPLES_FILE);
                        Log.d(TAG, "output file: " + GYRO_SAMPLES_FILE);
                        mPrintWriter = new PrintWriter(file);

                    } catch (FileNotFoundException e){
                        Log.e(TAG, "File not found exception");
                    }
                    mSensorManager.registerListener(onSensorChange, mGyroscope, SensorManager.SENSOR_DELAY_FASTEST);
                    // Sampling rate around 198.5Hz, we rounded to 200Hz
                    mRecordButton.setText(R.string.when_not_recording);
                }
                recording = !recording;
            }
        });
    }

    @Override
    protected void onResume(){
        super.onResume();

    }


    @Override
    protected void onPause(){
        super.onPause();

        //mPrintWriter.flush();
        // we should unregister the listener and make it into a service, todo
    }

    private SensorEventListener onSensorChange = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor arg0, int arg1) {
            // TODO Auto-generated method stub

        }

        @Override
        synchronized public void onSensorChanged(SensorEvent event) {
            Log.d(TAG, "Gyroscope update");
            mPrintWriter.println(event.timestamp + " " + event.values[0] + " " + event.values[1] + " " + event.values[2]);
        }
    };

    /**
     * Requests permissions if they weren't already granted
     * @param requestCode   code to identify which code section requested this
     * @param permissions   list of permissions to ask
     * @param grantResults  array containing the permission grant value from the User
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {}
            }
        }
    }

    /**
     * Auxiliary function used during development, took all the gyro recordings from the MALE
     * and FEMALE folders, transformed them into 3 wavs, 1 for each axis, extracts the features
     * from the wavs and packs them into a string to an output file used by svm.train(...);
     */
    private void convertAllToSVMFormat(){
        float[] features1, features2, features3;
        String[] fileList;
        File dir;
        File outputFile = new File(SVM_TRAIN_DIR+"/training_data.txt");
        String line = "";
        PrintWriter outWriter = null;
        try {
            outWriter = new PrintWriter(outputFile);
        } catch (IOException e){;}


        // Reading all male samples, converting to wav, obtaining features, then creating the file
        // used in training
        dir = new File(SVM_MALE_SAMPLES_DIR);
        fileList = dir.list();
        for (String s: fileList
                ) {
            String temp = s;
            s = SVM_MALE_SAMPLES_DIR + "/" + s;
            Utils.convertTXTtoWAV(s, SVM_MALE_OUT_WAV + "/" + temp, GYRO_SAMPLE_RATE, true);
            features1 = Utils.transformFeatures(mNativeMethods.getFeatures(SVM_MALE_OUT_WAV + "/" + temp + "_1.wav"));
            features2 = Utils.transformFeatures(mNativeMethods.getFeatures(SVM_MALE_OUT_WAV + "/" + temp + "_2.wav"));
            features3 = Utils.transformFeatures(mNativeMethods.getFeatures(SVM_MALE_OUT_WAV + "/" + temp + "_3.wav"));

            line = "0 ";
            int k=0;
            for(int i=0; i<features1.length; i++){
                line = line.concat(k + ":" + features1[i] + " ");
                k++;
            }
            for(int i=0; i<features2.length; i++){
                line = line.concat(k + ":" + features2[i] + " ");
                k++;
            }
            for(int i=0; i<features3.length; i++){
                line = line.concat(k + ":" + features3[i] + " ");
                k++;
            }

            outWriter.println(line);
        }

        dir = new File(SVM_FEMALE_SAMPLES_DIR);
        fileList = dir.list();
        for (String s: fileList
                ) {
            String temp = s;
            s = SVM_FEMALE_SAMPLES_DIR + "/" + s;
            Utils.convertTXTtoWAV(s, SVM_FEMALE_OUT_WAV + "/" + temp, GYRO_SAMPLE_RATE, true);
            features1 = Utils.transformFeatures(mNativeMethods.getFeatures(SVM_FEMALE_OUT_WAV + "/" + temp + "_1.wav"));
            features2 = Utils.transformFeatures(mNativeMethods.getFeatures(SVM_FEMALE_OUT_WAV + "/" + temp + "_2.wav"));
            features3 = Utils.transformFeatures(mNativeMethods.getFeatures(SVM_FEMALE_OUT_WAV + "/" + temp + "_3.wav"));

            line = "1 ";
            int k=0;

            for(int i=0; i<features1.length; i++){
                line = line.concat(k + ":" + features1[i] + " ");
                k++;
            }
            for(int i=0; i<features2.length; i++){
                line = line.concat(k + ":" + features2[i] + " ");
                k++;
            }
            for(int i=0; i<features3.length; i++){
                line = line.concat(k + ":" + features3[i] + " ");
                k++;
            }

            outWriter.println(line);
        }


        //This is horrible, but for some reasons it doesn't finish to write the last values if we don't put some kind of
        //delay artificially, it should be more or less a 20ms delay
        for(int i=5000; i>0; i--) ;

        outWriter.close();
    }

    /**
     * Scales data from the training file and trains the corresponding model
     */
    private void trainSVM(){
        svm.scale(SVM_TRAIN_DIR + "/training_data.txt", SVM_TRAIN_DIR + "/training_data_scaled.txt");
        svm.train("-t 2 " + SVM_TRAIN_DIR + "/training_data_scaled.txt " + SVM_TRAIN_DIR + "/trained_model.txt");

    }

    /**
     * Used in development, in the log we obtain an accuracy value corresponding to the % of lines of the training set
     * that were correctly classified
     */
    private void getTrainAccuracy(){
        svm.predict(SVM_TRAIN_DIR + "/training_data_scaled.txt " + SVM_TRAIN_DIR+"/trained_model.txt " + SVM_TRAIN_DIR + "/predicted_class.txt");
    }

    /**
     * Used in development, in the log we obtain an accuracy value corresponding to the % of lines of the validation
     * set that were correctly classified
     */
    private void validateModel(){
        float[] features1, features2, features3;
        String[] fileList;
        File dir;
        File outputFile = new File(SVM_TRAIN_DIR+"/training_data.txt");
        String line = "";
        PrintWriter outWriter = null;
        try {
            outWriter = new PrintWriter(new FileWriter(outputFile, true));
        } catch (IOException e){;}


        // Reading all male samples, converting to wav, obtaining features, then creating the file
        // used in training
        dir = new File(SVM_MALE_SAMPLES_DIR+"_predict");
        fileList = dir.list();
        for (String s: fileList
                ) {
            String temp = s;
            s = SVM_MALE_SAMPLES_DIR + "_predict/" + s;
            Utils.convertTXTtoWAV(s, SVM_MALE_OUT_WAV + "/" + temp, GYRO_SAMPLE_RATE, true);
            features1 = Utils.transformFeatures(mNativeMethods.getFeatures(SVM_MALE_OUT_WAV + "/" + temp + "_1.wav"));
            features2 = Utils.transformFeatures(mNativeMethods.getFeatures(SVM_MALE_OUT_WAV + "/" + temp + "_2.wav"));
            features3 = Utils.transformFeatures(mNativeMethods.getFeatures(SVM_MALE_OUT_WAV + "/" + temp + "_3.wav"));

            line = "0 ";
            int k=0;
            for(int i=0; i<features1.length; i++){
                line = line.concat(k + ":" + features1[i] + " ");
                k++;
            }
            for(int i=0; i<features2.length; i++){
                line = line.concat(k + ":" + features2[i] + " ");
                k++;
            }
            for(int i=0; i<features3.length; i++){
                line = line.concat(k + ":" + features3[i] + " ");
                k++;
            }

            outWriter.println(line);
        }

        dir = new File(SVM_FEMALE_SAMPLES_DIR+"_predict");
        fileList = dir.list();
        for (String s: fileList
                ) {
            String temp = s;
            s = SVM_FEMALE_SAMPLES_DIR + "_predict/" + s;
            Utils.convertTXTtoWAV(s, SVM_FEMALE_OUT_WAV + "/" + temp, GYRO_SAMPLE_RATE, true);
            features1 = Utils.transformFeatures(mNativeMethods.getFeatures(SVM_FEMALE_OUT_WAV + "/" + temp + "_1.wav"));
            features2 = Utils.transformFeatures(mNativeMethods.getFeatures(SVM_FEMALE_OUT_WAV + "/" + temp + "_2.wav"));
            features3 = Utils.transformFeatures(mNativeMethods.getFeatures(SVM_FEMALE_OUT_WAV + "/" + temp + "_3.wav"));

            line = "1 ";
            int k=0;

            for(int i=0; i<features1.length; i++){
                line = line.concat(k + ":" + features1[i] + " ");
                k++;
            }
            for(int i=0; i<features2.length; i++){
                line = line.concat(k + ":" + features2[i] + " ");
                k++;
            }
            for(int i=0; i<features3.length; i++){
                line = line.concat(k + ":" + features3[i] + " ");
                k++;
            }


            outWriter.println(line);
        }


        //This is horrible, but for some reasons it doesn't finish to write the last values if we don't put some kind of
        //delay artificially, it should be more or less a 20ms delay
        for(int i=5000; i>0; i--) ;

        outWriter.close();

        svm.scale(SVM_TRAIN_DIR + "/training_data.txt", SVM_TRAIN_DIR + "/predict_scaled.txt");
        svm.predict(SVM_TRAIN_DIR + "/predict_scaled.txt " + SVM_TRAIN_DIR+"/trained_model.txt " + SVM_TRAIN_DIR + "/predicted_class.txt");
        file = new File(SVM_TRAIN_DIR+"/predicted_class.txt");

        Scanner scan = null;
        try {
            scan = new Scanner(file);
        }catch(FileNotFoundException e){};

        int m, f, t;
        m = f = t = 0;
        String temp = null;
        while(scan.hasNext()){
            temp = scan.next();
            t++;
            if(temp.charAt(0) == '0'){
                m++;
            } else if(temp.charAt(0) == '1'){
                f++;
            }
        }


        Log.d(TAG, "Male probability: " + (((float)m)/t)*100 + "%, Female probability: " + (((float)f)/t)*100 + "%");
    }


    /**
     * Function which evaluates the class of the last gyroscope recording
     * @return  "Male", "Female" based on the classification
     */
    private String predictClassOf(){
        PrintWriter outWriter = null;
        Scanner scan = null;

        File outputFile = new File(GYRO_SAMPLES_DIR+"/prediction_data.txt");
        try {
            scan = new Scanner(new File(GYRO_SAMPLES_DIR+"/training_data.txt"));
        } catch (IOException e){;}

        try {
            outWriter = new PrintWriter(outputFile);
        } catch (IOException e){;}


        while(scan.hasNextLine()){
            String t = scan.nextLine();
            Log.d(TAG, "predictClassOf: " + t);
            outWriter.println(t);
        }
        scan.close();

        Utils.convertTXTtoWAV(GYRO_SAMPLES_FILE, GYRO_SAMPLES_DIR+"/gyro_samples",200, true);
        float[] features1 = Utils.transformFeatures(mNativeMethods.getFeatures(GYRO_SAMPLES_DIR + "/gyro_samples_1.wav"));
        float[] features2 = Utils.transformFeatures(mNativeMethods.getFeatures(GYRO_SAMPLES_DIR + "/gyro_samples_2.wav"));
        float[] features3 = Utils.transformFeatures(mNativeMethods.getFeatures(GYRO_SAMPLES_DIR + "/gyro_samples_3.wav"));

        String line = "";

        line = "-1 ";
        int k=0;

        for(int i=0; i<features1.length; i++){
            line = line.concat(k + ":" + features1[i] + " ");
            k++;
        }
        for(int i=0; i<features2.length; i++){
            line = line.concat(k + ":" + features2[i] + " ");
            k++;
        }
        for(int i=0; i<features3.length; i++){
            line = line.concat(k + ":" + features3[i] + " ");
            k++;
        }

        outWriter.println(line);
        //This is horrible, but for some reasons it doesn't finish to write the last values if we don't put some kind of
        //delay artificially, it should be more or less a 20ms delay
        for(int i=30000; i>0; i--);

        outWriter.close();
        svm.scale(GYRO_SAMPLES_DIR + "/prediction_data.txt", GYRO_SAMPLES_DIR + "/predict_scaled.txt");
        svm.predict(GYRO_SAMPLES_DIR + "/predict_scaled.txt " + GYRO_SAMPLES_DIR +"/trained_model.txt " + GYRO_SAMPLES_DIR + "/predicted_class.txt");
        file = new File(GYRO_SAMPLES_DIR+"/predicted_class.txt");

        try {
            scan = new Scanner(file);
        }catch(FileNotFoundException e){};

        int m, f, t;
        m = f = t = 0;
        String temp = null;
        while(scan.hasNextLine()){
            temp = scan.nextLine();
            t++;
            if(temp.charAt(0) == '0'){
                m++;
            } else if(temp.charAt(0) == '1'){
                f++;
            }
        }

        Log.d(TAG,"Last one was: " + temp.charAt(0));
        if(temp.charAt(0) == '0'){
            return "Male";
        } else {
            return "Female";
        }
    }
}
