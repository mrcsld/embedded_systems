package it.unipd.dei.gyrosniffer;

import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Scanner;

import umich.cse.yctung.androidlibsvm.LibSVM;

/**
 * Created by marco on 27/08/18.
 */

public class Utils {

    private final static String TAG = "it.unipd.dei.gyrosniffer.utils: ";

    public static short convertDoubleToPCM(double value){
        value *= 32768.0;
        value = Math.max(value, Short.MIN_VALUE);
        value = Math.min(value, Short.MAX_VALUE);
        return (short)value;
    }

    public static  void convertTXTtoWAV(String txtPath, String wavPath, int samplerate, boolean hasTimestamp){
        try {
            File readFile = new File(txtPath);
            long ciao = readFile.length();
            Scanner scan = new Scanner(readFile);
            ArrayList<Float> readings = new ArrayList<>();

            ArrayList<Double> ax1 = new ArrayList<>();
            ArrayList<Double> ax2 = new ArrayList<>();
            ArrayList<Double> ax3 = new ArrayList<>();

            double d1,d2,d3;

            // This creates a vector for each axis, if we don't have a full scan of the 3 axis for the last reading it gets truncated
            if(hasTimestamp) {
                while(scan.hasNextLong()){
                    scan.nextLong();
                    if (scan.hasNext()) {
                        d1 = Double.parseDouble(scan.next());
                        if (scan.hasNext()) {
                            d2 = Double.parseDouble(scan.next());
                            if (scan.hasNext()) {
                                d3 = Double.parseDouble(scan.next());

                                ax1.add(d1);
                                ax2.add(d2);
                                ax3.add(d3);
                            }
                        }
                    }
                }
            } else {
                while (scan.hasNext()) {
                    d1 = Double.parseDouble(scan.next());
                    if (scan.hasNext()) {
                        d2 = Double.parseDouble(scan.next());
                        if (scan.hasNext()) {
                            d3 = Double.parseDouble(scan.next());

                            ax1.add(d1);
                            ax2.add(d2);
                            ax3.add(d3);
                        }
                    }
                }
            }

            writeWAV(wavPath+"_1.wav", ax1, samplerate);
            writeWAV(wavPath+"_2.wav", ax2, samplerate);
            writeWAV(wavPath+"_3.wav", ax3, samplerate);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Writes a WAV file to the output, with a given samplerate (in this case we upsample the
     * signal by a factor 40, so to 8KHz, to do so we repeat 40 times each sample)
     * @param outPath File where the WAVE file will be written
     * @param values List of pcm readings (in our case the gyroscope values)
     * @param samplerate Samplerate of the readings (in our case 200)
     */
    private static void writeWAV(String outPath, ArrayList<Double> values, int samplerate) {
        try{

            int myDatasize = (values.size() - 80) * Short.SIZE / 8 * 40; // we trim 200ms at the start
                                                                         // and at the end

            /** Data for creating "The canonical WAVE file format"
             http://soundfile.sapp.org/doc/WaveFormat/
             */
            int BitsPerSample = 16;

            String ChunkID = "RIFF";
            String Format = "WAVE";
            String Subchunk1ID = "fmt ";
            int mySubChunk1Size = 16;
            short AudioFormat = 1;
            short NumChannels = 1;
            int SampleRate = samplerate*40;
            int ByteRate = SampleRate * ((int) NumChannels) * BitsPerSample / 8;
            short BlockAlign = (short) (NumChannels * ((short) (BitsPerSample / 8)));
            String Subchunk2ID = "data";
            int Subchunk2Size = (int) (myDatasize);
            int chunkSize = 36 + Subchunk2Size;

            OutputStream os;
            os = new FileOutputStream(new File(outPath));
            BufferedOutputStream bos = new BufferedOutputStream(os);
            DataOutputStream outFile = new DataOutputStream(bos);

            outFile.writeBytes(ChunkID); // The riff chunk descriptor
            outFile.writeInt(Integer.reverseBytes(chunkSize));
            outFile.writeBytes(Format);
            outFile.writeBytes(Subchunk1ID); // The fmt sub-chunk
            outFile.writeInt(Integer.reverseBytes(mySubChunk1Size));
            outFile.writeShort(Short.reverseBytes(AudioFormat));
            outFile.writeShort(Short.reverseBytes(NumChannels));
            outFile.writeInt(Integer.reverseBytes(SampleRate));
            outFile.writeInt(Integer.reverseBytes(ByteRate));
            outFile.writeShort(Short.reverseBytes(BlockAlign));
            outFile.writeShort(Short.reverseBytes((short) BitsPerSample));
            outFile.writeBytes(Subchunk2ID); // The "data" sub-chunk
            outFile.writeInt(Integer.reverseBytes(Subchunk2Size));


            int size = values.size();
            double val;
            // we trim 200ms at the start and at the end
            for(int i=40; i<size-40; i++){
                val=values.get(i);
                for(int j=0; j<40; j++){
                    outFile.writeShort(Short.reverseBytes(Utils.convertDoubleToPCM(val)));
                }
            }


            outFile.flush();
            outFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Obtains mean values of the feature arrays (of each feature over all the readings)
     * @param values a list of feature arrays
     * @return mean values
     */
    private static float[] getMean(ArrayList<float[]> values){
        float[] means = new float[values.get(0).length];
        for(int i=0; i<means.length; i++){
            means[i] = 0;
        }

        int num = values.size();

        for(float[] vector : values){
            for(int i=0; i<vector.length; i++){
                means[i] += vector[i];
            }
        }

        for(int i=0; i<means.length; i++){
            means[i] = means[i]/num;
        }

        return means;
    }

    /**
     * Calculates the mean of each index i of the internal vector
     * @param values ArrayList of float vectors
     * @return array of mean values
     */
    private static float[] getVariance(ArrayList<float[]> values){
        float[] means = getMean(values);
        ArrayList<float[]> tmp = values;
        for(float[] vector:tmp){
            for(int i=0; i<vector.length; i++){
                vector[i] = (float)Math.pow(vector[i]-means[i],2);
            }
        }
        return getMean(tmp);
    }

    /**
     * Calculates the Delta vector of the mfcc values
     * @param values ArrayList of mfcc vectors
     * @return Delta vector
     */
    private static ArrayList<float[]> getAbsoluteDeltaVector(ArrayList<float[]> values){
        ArrayList<float[]> delta = new ArrayList<>();
        float[] tmp;

        int size = values.size()-1;
        int size_internal = values.get(0).length;
        tmp = new float[size_internal];
        for(int i=0; i<size; i++){
            for(int j=0; j<size_internal; j++){
                tmp[j] = Math.abs((values.get(i))[j] - (values.get(i+1))[j]);
            }
            delta.add(tmp);
        }

        return delta;
    }

    /**
     * Obtains the vector of the maximum value for each mfcc coefficient
     * @param values ArrayList of mfcc vectors
     * @return array of mfcc maximum values
     */
    private static float[] getMaximum(ArrayList<float[]> values){
        float[] maximum = values.get(0);
        int size = values.size();
        float[] tmp;
        for(int i=1; i<size; i++){
            tmp = values.get(i);
            for(int j=0; j<maximum.length; j++){
                maximum[j] = Math.max(maximum[j], tmp[j]);
            }
        }

        return maximum;
    }

    /**
     * Obtains the vector of the minimum value for each mfcc coefficient
     * @param values ArrayList of mfcc vectors
     * @return array of mfcc minimum values
     */
    private static float[] getMinimum(ArrayList<float[]> values){
        float[] minimum = values.get(0);
        int size = values.size();
        float[] tmp;
        for(int i=1; i<size; i++){
            tmp = values.get(i);
            for(int j=0; j<minimum.length; j++){
                minimum[j] = Math.min(minimum[j], tmp[j]);
            }
        }

        return minimum;
    }

    /**
     * Transforms the ArrayList of mfcc vectors to the format used for the training
     * @param values ArrayList of mfcc vectors
     * @return array of features obtained from the values
     */
    public static float[] transformFeatures(ArrayList<float[]> values){
        ArrayList<float[]> delta = getAbsoluteDeltaVector(values);
        float[] mean = getMean(values);
        float[] variance = getVariance(values);
        float[] mean_delta = getMean(delta);
        float[] variance_delta = getVariance(delta);
        float[] maximum = getMaximum(values);
        float[] minimum = getMinimum(values);
        float[] toReturn = new float[mean.length*6];
        int j=0;
        for(float f:mean){
            toReturn[j] = f;
            j++;
        }
        for(float f:variance){
            toReturn[j] = f;
            j++;
        }
        for(float f:mean_delta){
            toReturn[j] = f;
            j++;
        }
        for(float f:variance_delta){
            toReturn[j] = f;
            j++;
        }
        for(float f:maximum){
            toReturn[j] = f;
            j++;
        }
        for(float f:minimum){
            toReturn[j] = f;
            j++;
        }

        return toReturn;
    }
}
