package it.unipd.dei.gyrosniffer;

import java.util.ArrayList;

/**
 * Created by marco on 21/08/18.
 */

public class NativeMethods {
    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }


    //////////////////////////////////////////////////////
    // Methods from native-lib
    //////////////////////////////////////////////////////
    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native ArrayList<float[]> getFeatures(String path);
}
