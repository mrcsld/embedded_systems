function SVMModel = trainSVMclassifier(input_matrix ,input_label_vector)
        
        SVMModel = fitcsvm(input_matrix,input_label_vector);
    
end