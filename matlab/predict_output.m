function test_model = predict_output(SVM_model, input.txt)
    
    input_wav = gyro_record_to_wav(input.txt, output_dir, 200);
    mfcc_in = calc_mfcc(input_wav, 200);
    label = predict(SVM_model, mfcc_in);
    print(label);
end