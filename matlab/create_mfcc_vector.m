function out_matrix = create_mfcc_vector(wav_dir)


 P = wav_dir;
 S = dir(fullfile(P,'*.wav'));
 N = natsortfiles({S.name});
 out_size = size(N);
 columns = 78;
 out_matrix = zeros(out_size(2), 78);
 C = cell(size(N));
 D = cell(size(N));
 for k = 1:numel(N)
     str = fullfile(P,N{k});
     [C{k},D{k}] = audioread(str, 'double');
    temp = transpose(calc_mfcc(C{k}, D{k}));
      out_matrix(k, 1:78) = temp;
  end

end