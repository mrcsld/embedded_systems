function out_vector = create_rms_vector(wav_dir)
% This function evaluates the rms energy of the wav samples which is another feature on which we can train the SVM model to get a realiable prediction on the gender of the speaker

 P = wav_dir;
 S = dir(fullfile(P,'*.wav'));
 N = natsortfiles({S.name});
 out_size = size(N);
 
 C = cell(size(N));

 for k = 1:numel(N)
     str = fullfile(P,N{k});
     [C{k}] = audioread(str);
    RMS = sqrt(mean(C{k}.^2, 1));
      out_vector(k) = RMS;
 end
 out_vector = transpose(out_vector);
end