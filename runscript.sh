#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
cd $DIR/app/src/main/jni
/home/marco/Android/Sdk/ndk-bundle/ndk-build

mv ../libs/x86/libgnustl_shared.so ../jniLibs/x86/libgnustl_shared.so
mv ../libs/x86/libnative-lib.so ../jniLibs/x86/libnative-lib.so

mv ../libs/armeabi-v7a/libgnustl_shared.so ../jniLibs/armeabi-v7a/libgnustl_shared.so
mv ../libs/armeabi-v7a/libnative-lib.so ../jniLibs/armeabi-v7a/libnative-lib.so

rm -r ../libs
rm -r ../obj