COMPILING INSTRUCTION (Android Studio)
    ** JUST FIRST TIME **
        Edit the file runscript.sh with the path to your ndk-build (the third line)
           -> chmod +x runscript.sh

    ** Before compiling in Android Studio, every time we apport changes to the cpp files
        -> ./runscript.sh
        Then you can simply run the program, remember to add storage permission to the app (you still need to do it manually, just the first time if you don't uninstall the app)


RUNNING INSTRUCTIONS (APK)
  ** JUST FIRST TIME **
        Copy the files trained_model.txt and training_data.txt to the system external storage main
            directory (if you don't have any put them in the local memory root instead)

  The application needs storage permissions to run, give them when the popup appears
